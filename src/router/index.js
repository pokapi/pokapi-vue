import Vue from 'vue'
import Router from 'vue-router'
import home from '@/components/home'
import pokemon from '@/components/pokemon'
import type from '@/components/type'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: home
    },
    {
      path: '/pokemon/:name',
      name: 'pokemon',
      component: pokemon
    },
    {
      path: '/type/:type',
      name: 'type',
      component: type
    }
  ]
})
