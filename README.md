# Pokapi

> A small school project with Samy and Victor

## Build Setup

``` bash
# install dependencies
$ npm install
```

To launch this project you will need [Python]('https://www.python.org/') and the [Flask]('https://flask.palletsprojects.com/en/1.1.x/installation/#install-flask') library

```bash
# install flask with pip
$ pip install Flask
```

## Start project

To run API server

```bash
# It listen on localhost:8888
$ python app.py
```

To run VueJS project
```bash
# serve with hot reload at localhost:8080
npm run dev 
```
